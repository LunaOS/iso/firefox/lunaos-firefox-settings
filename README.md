# Firefox settings for LunaOS

## **Sources used in LunaOS Firefox Settings:**

* Customizations based on customized [Betterfox](https://github.com/yokoffing/Betterfox)

* The theme uses css files from 
	* [firefox-csshacks](https://github.com/MrOtherGuy/firefox-csshacks/tree/master)
	* [ShyFox](https://github.com/Naezr/ShyFox)
	* [slick-fox](https://github.com/Etesam913/slick-fox)
